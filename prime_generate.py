from random import Random
from math import gcd
from time import time


RANDOM = Random()


def simple_check_prime(n: int):
    primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103,
              107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223,
              227, 229, 233, 239, 241, 251]
    return n in primes or all(n % prime != 0 for prime in primes)


def fermat_prime(n: int, iterations_count: int):
    checked_numbers = set()

    for i in range(iterations_count):
        a = RANDOM.randint(2, n - 2)
        while len(checked_numbers) < n - 3 and a in checked_numbers:
            a = RANDOM.randint(2, n - 2)
        checked_numbers.add(a)

        if gcd(a, n) != 1:
            return False
        if pow(a, n - 1, n) != 1:
            return False

    return True


def bbs_generate(n: int, x0=71, p=2047, q=8191):
    curr = x0
    number = 1
    for i in range(n - 1):
        curr = (curr ** 2) % (p * q)
        number |= ((curr & 1) << (i + 1))
    number |= (1 << (n - 1))
    return number


def largest_number(bits: int):
    number = 0
    for i in range(bits):
        number |= (1 << i)
    return number


def generate_prime_number(n: int, iterations: int):
    number = bbs_generate(n)
    is_fermat_prime = fermat_prime(number, iterations)
    print(f'Тест Ферма: число {number} {f"простое с вероятностью {1 - 1 / pow(2, iterations)}" if is_fermat_prime else "составное"}')

    start_time = time()
    taken_iterations = 0
    largest_number_with_n_bits = largest_number(n)
    while not simple_check_prime(number) or not is_fermat_prime:
        number = (number + 2) % largest_number_with_n_bits
        is_fermat_prime = fermat_prime(number, iterations)
        print(f'Тест Ферма: число {number} {f"простое с вероятностью {1 - 1 / pow(2, iterations)}" if is_fermat_prime else "составное"}')
        taken_iterations += 1
    end_time = time()

    return number, taken_iterations, end_time - start_time


def main():
    n = int(input('Введите количество битов в простом числе: '))
    if n < 3:
        print('Количество битов должно быть больше 2')
        return

    number, iterations, exec_time = generate_prime_number(n, 100)
    print(f'Простое число: {number}\nКоличество итераций: {iterations}\nВремя: {exec_time} c.')


if __name__ == '__main__':
    main()
